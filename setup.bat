ECHO installing packages...
CALL npm i

ECHO installing process manager...
CALL npm i pm2 -g

ECHO installing startup process...
CALL npm install pm2-windows-startup -g

ECHO Running script...
CALL pm2 start  %~dp0index.js
CALL pm2 save

CALL pm2-startup install

PAUSE