class PaymentUtil {

  makeTransactionRequest(transactionId, amount, type, merchantId) {
    let sendData = "5632";
    let payload = "";
    if (type == "PR") {
      payload = type + "," + transactionId + "," + merchantId + "," + amount;
    } else if (type == "RS?") {
      payload = type + "," + transactionId + "," + merchantId;
    }
    let length = payload.length;
    let hexString = length.toString(16);
    let result = this.asciitohex(payload);
    
    // var lengths = result.length;
    // let lst = [];
    // for (let j = 0; j < lengths; j = j + 2) {
    //   try {
    //     lst.push(parseInt(result.substring(j, j + 2), 16));
    //   } catch (e) {
    //     //   
    //   }
    // }

    let lst = Buffer.from(result, 'hex');
    let fst = true;
    let lastxor;
    console.log(lst);
    for (let xorData in lst) {
      console.log(xorData)
      if (fst) {
        lastxor = xorData;
        fst = false;
      } else {
        let xResult = lastxor  ^ xorData;
        lastxor = xResult;
        console.log(lastxor);
      }
    }

    console.log(lastxor);

    var twosCompliment = this.findTwosComplement(hexString);
    var lastString = sendData + twosCompliment + result + lastxor;

    return lastString;
  }

  findTwosComplement(length) {
    var totalLenth = length.length;
    let buffer = "";
    var paddedZeros = 4 - totalLenth;
    for (let i = 0; i < paddedZeros; i++) {
      buffer = buffer + "0";
    }
    buffer = buffer + length;
    return buffer;
  }

  convertedString(data) {
    let res = "";
    for (var da in data) {
      var p2s = da.toRadixString(16);
      if (p2s.length == 1) {
        p2s = "0" + p2s;
      }
      res = res + p2s;
    }
    return res;
  }

  hextoascii(hexString) {
    return new Buffer.from(hexString, 'hex');
  }

  asciitohex(payload) {
    return new Buffer.from(payload, 'ascii').toString('hex');
  }
}

module.exports = PaymentUtil;
