const express = require("express");
var bodyParser = require("body-parser");
var cors = require("cors");
var net = require("net");

const app = express();
const port = 3000;

app.use(cors());
app.use(bodyParser.json());

app.get("/", (req, res) => res.send("The driver is working fine!"));

app.post("/", (req, res) => {
  try {
    var { ip, port, data } = req.body;

    var client = new net.Socket();
    client.setTimeout(5000, () => {
      console.error("Timeout");
      client.destroy();
      res.status(408).json({status: 0})
    })
    client.connect(port, ip, function () {
      console.log("Connected");
      client.write(new Uint8Array(data));
      client.destroy();
      res.status(200).json({status: 1})
    }).on('error' , (err) => {
      client.destroy();
      console.log(err);
      res.status(408).json({status: 0})
    });

    // res.send("success");
  } catch (error) {
    console.log(error);
    res.status(404).json({status: 0})
  }
});

app.post("/payment", (req, res) => {
  try {
    var { ip, port, request, status } = req.body;

    console.log(ip);
    console.log(port);
    console.log(request);
    console.log(status);

    var client = new net.Socket();
    client.connect(port, ip, async () => {
      console.log("Connected");
      setTimeout(() => {
        client.destroy();
        client.end();
      }, 60000);
      client.write(new Uint8Array(request));

      client.write(new Uint8Array(status));
    });

    client.on("data", (data) => {
      console.log(data.toString("utf8"));

      let final = data.toString("utf8");
      let formatted = final.substring(3, final.length - 2);

      console.log(formatted);
      console.log(formatted.split(",")[3]);

      if (formatted.split(",")[3] == "??") {
        setTimeout(() => {
          client.write(new Uint8Array(status));
        }, 1000);
      } else {
        client.end();
        client.destroy();
        return res.send(formatted);
      }
    });

    client.on("error", (err) => {
      console.log(err);
      client.end();
      client.destroy();
      if(err.code == 'EADDRNOTAVAIL') {
        return res.status(404).send('connection  failed');
      }
      if(err.code == 'ECONNREFUSED') {
        return res.status(404).send('connection  failed');
      }
      return res.status(408).send('disconnected');
    });
  } catch (error) {
    console.log(error);
    return res.status(404).send('connection  failed');
  }
});

app.post("/paycheck", (req, res) => {
  try {
    var { ip, port, request } = req.body;

    console.log(ip);
    console.log(port);
    console.log(request);

    var client = new net.Socket();
    client.connect(port, ip, async () => {
      console.log("Connected");
      setTimeout(() => {
        client.end();
        client.destroy();
      }, 60000);
      client.write(new Uint8Array(request));
    });

    client.on("data", (data) => {
      console.log(data.toString("utf8"));

      let final = data.toString("utf8");
      // let formatted = final.substring(3, final.length - 2);

      // console.log(formatted);
      // console.log(formatted.split(",")[3]);

      // if (formatted.split(",")[3] == "??") {
      //   setTimeout(() => {
      //     client.write(new Uint8Array(status));
      //   }, 1000);
      // } else {
        client.end();
        client.destroy();
        return res.send(final);
      // }
    });

    client.on("error", (err) => {
      console.log(err);
      client.end();
      client.destroy();
      if(err.code == 'EADDRNOTAVAIL') {
        return res.status(404).send('connection  failed');
      }
      if(err.code == 'ECONNREFUSED') {
        return res.status(404).send('connection  failed');
      }
      return res.status(408).send('disconnected');
    });
  } catch (error) {
    console.log(error);
    return res.status(404).send('connection  failed');
  }
});

app.listen(port, () => console.log(`Driver is running successfully`));
